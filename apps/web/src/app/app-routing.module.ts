import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { Component2Component } from './component2/component2.component';

const routes: Routes = [
    {
        path: '',
        redirectTo:"/compontent2",
        pathMatch: 'full',
    },
    {
        path: 'compontent2',
        component: Component2Component,
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
