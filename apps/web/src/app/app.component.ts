import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'tymetro-testing-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  title = 'web';
  res!: string
  constructor(private _http: HttpClient) { }

  testGet() {
    this._http.get(`${environment.apiUrl}/api`).subscribe(
      x => this.res = JSON.stringify(x)
    )
  }
}
