export const environment = {
  production: false,
  port: 51000,
  neo4j_host: process.env.NEO4J_HOST
};
